<?php

namespace app\controllers;

use Yii;
use app\models\admin\AdminLoginForm;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\User;
use app\models\admin\AdminEditUserForm;
use app\models\admin\AdminCreateUserForm;
use app\models\Ticket;
use app\models\admin\AdminOpenTicket;
use app\models\admin\AdminTicketComment;
use yii\data\Pagination;

class AdminController extends Controller
{
    /**
     * separate layouts for admin dashboard
     *
     * @var string
     */
    public $layout = '@app/views/admin/layouts/main.php';

    /**
     * access to admin page only admin
     *
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'except' => ['login'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
                'denyCallback' => function () {
                    $this->redirect(['admin/login']);
                }
            ]
        ];
    }

    /**
     * Index action
     *
     * @return string
     */
    public function actionIndex()
    {
        $query = User::find();
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->defaultPageSize = 5;
        $users = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('index', ['users' => $users, 'pages' => $pages,]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = '@app/views/admin/layouts/main-login';
        $model = new AdminLoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->redirect(['admin/index']);
        }
        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     *Edit user action
     *
     * @param $id
     * @return  string the rendering result.
     */
    public function actionEditUser($id)
    {
        $model = new AdminEditUserForm();
        $model->setUserData($id);
        if ($model->load(Yii::$app->request->post())) {
            $session = Yii::$app->session;
            $session->setFlash('userEdited', 'You have successfully edited user');
            $model->edit($id);
        }
        $model->password = '';
        return $this->render('edit-user', ['model' => $model]);
    }

    /**
     * Create user action
     *
     * @param $id
     * @return string
     */
    public function actionCreateUser()
    {
        $model = new AdminCreateUserForm();
        if ($model->load(Yii::$app->request->post()) && $model->create()) {
            $session = Yii::$app->session;
            $session->setFlash('userCreated', 'You have successfully created user');
            $model = new AdminCreateUserForm();
        }
        $model->password = '';
        return $this->render('create-user', ['model' => $model]);
    }

    /**
     * Action delete user
     *
     */
    public function actionDeleteUser($id)
    {
        $user = User::findOne($id);
        $userRoles = Yii::$app->authManager->getRolesByUser($id);
        $session = Yii::$app->session;
        if (!isset($userRoles['admin']) && $user->delete()) {
            $session->setFlash('userDeleted', 'You have successfully deleted user');
            return $this->redirect(['admin/index']);
        } else {
            $session->setFlash('userNotDeleted', 'Admin can\'t be deleted');
            return $this->redirect(['admin/edit-user', 'id' => $id]);
        }
    }

    /**
     * Show all tickets
     *
     */
    public function actionTickets()
    {
        $tickets = Ticket::find()->orderBy(['created_at' => SORT_DESC])->all();
        return $this->render('tickets', ['tickets' => $tickets]);
    }

    /**
     * Open ticket
     *
     */
    public function actionOpenTicket($id)
    {
        $model = new AdminOpenTicket();
        $model->setData($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->changeStatus();
        }
        $ticket = Ticket::findOne($id);
        $commentModel = new AdminTicketComment;
        return $this->render('open-ticket', ['ticket' => $ticket, 'model' => $model, 'commentModel' => $commentModel]);
    }

    /**
     * Save tickets comment
     */
    public function actionSaveComment($id)
    {
        $model = new AdminTicketComment;
        if ($model->load(Yii::$app->request->post())) {
            $model->save($id);
        }
        return $this->redirect(['admin/open-ticket', 'id' => $id]);
    }
}
