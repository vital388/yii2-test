<?php
namespace app\assets;

use yii\web\AssetBundle;

class DataTableAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/bower_components/datatables.net';
    public $js = [
        'js/jquery.dataTables.min.js',
        // more plugin Js here
    ];
    public $depends = [
        'dmstr\web\AdminLteAsset',
    ];
}