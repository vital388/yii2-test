<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class TicketComment extends ActiveRecord
{
    /**
     * Add timestamp to created field
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new Expression('NOW()')
            ],
        ];
    }

    /**
     * Get user name
     *
     */
    public function getUserName()
    {
        $row = (new \yii\db\Query())
            ->select(['user.name'])
            ->from('ticket')
            ->innerJoin('user', 'user.id = ticket.user_id')
            ->where(['ticket.id' => $this->ticket_id])
            ->one();

        if (empty($row)) {
            $error = 'User not found';
            throw new \Exception($error);
        }
        return $row['name'];
    }
}