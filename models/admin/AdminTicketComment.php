<?php

namespace app\models\admin;

use Yii;
use yii\base\Model;
use app\models\Ticket;
use app\models\TicketComment;

/**
 * Class AdminTicketComment
 * @package app\models\admin
 */
class AdminTicketComment extends Model
{
    public $message;

    public function rules()
    {
        return [
            [['message'], 'required']
        ];
    }

    public function save($id)
    {
        $ticketComment = new TicketComment();
        $ticketComment->message = $this->message;
        $ticketComment->ticket_id = $id;
        $ticketComment->type = 'admin';
        $ticketComment->save();
    }
}
