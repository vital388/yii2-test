<?php

namespace app\models\admin;

use Yii;
use yii\base\Model;
use app\models\Ticket;

/**
 * Class AdminOpenTicket
 * @package app\models\admin
 */
class AdminOpenTicket extends Model
{
    public $status;
    public $ticket;

    public function rules()
    {
        return [
            [['status'], 'required']
        ];
    }

    public function changeStatus()
    {
        $this->ticket->status = $this->status;
        return $this->ticket->update();
    }

    public function setData($id)
    {
        $this->ticket = Ticket::findOne($id);
        $this->status = $this->ticket->status;
    }


}
