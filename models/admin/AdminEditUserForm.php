<?php

namespace app\models\admin;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Class AdminEditUserForm
 * @package app\models\admin
 */
class AdminEditUserForm extends Model
{
    public $name;
    public $password;
    public $email;
    public $userId;

    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'New password',
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'password', 'email'], 'required'],
            ['email', 'email'],
            [['email', 'password', 'name'], 'string', 'min' => 6],
            [['email', 'password', 'name'], 'string', 'max' => 60],
            ['name', 'firstLatterCapital'],
            ['password', 'lowercase'],
            ['email', 'unique', 'targetClass' => '\app\models\User',
                'when' => function ($model) {
                    return $model->email != User::findIdentity($this->userId)->email;
                }
            ]
        ];
    }

    /**
     * Check if name has capital first letter
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function firstLatterCapital($attribute, $params, $validator)
    {
        if (!preg_match('/^[A-Z]+/', $this->$attribute)) {
            $validator->addError($this, $attribute, "The $attribute must start from capital letter");
        }
    }

    /**
     * Check if password in lower case
     *
     * @param $attribute
     * @param $params
     * @param $validator
     */
    public function lowercase($attribute, $params, $validator)
    {
        if (!preg_match('/^[a-z0-9_@]+$/', $this->$attribute)) {
            $validator->addError($this, $attribute, "The $attribute must be in lower case");
        }
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function edit($userId)
    {
        if ($this->validate()) {
            $user = User::findOne($userId);
            $user->name = $this->name;
            $user->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $user->email = $this->email;
            $user->update();
            return $user;
        }
        $this->password = '';
        return null;
    }

    /**
     * Set user data to model
     *
     * @param $userId
     */
    public function setUserData($userId)
    {
        $user = User::findOne($userId);
        $this->name = $user->name;
        $this->email = $user->email;
        $this->userId = $userId;
        $this->password = '';
    }
}
