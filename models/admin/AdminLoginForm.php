<?php

namespace app\models\admin;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Class AdminLoginForm
 * @package app\models\admin
 */
class AdminLoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;
    private $_user = null;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],

        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user) {
                $this->addError($attribute, 'Incorrect email or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }
        return false;
    }

    /**
     * Get user if credentials are right
     *
     * @return \yii\db\ActiveRecord|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::find()
                ->where(['email' => $this->email])
                ->one();

            if (!$this->_user || !Yii::$app->getSecurity()->validatePassword($this->password, $this->_user->password)) {
                return null;
            }

        }

        return $this->_user;
    }
}
