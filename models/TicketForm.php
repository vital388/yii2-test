<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Ticket;

/**
 * Class TicketForm
 * @package app\models
 */
class TicketForm extends Model
{
    public $title;
    public $description;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        $ticket = new Ticket();
        $ticket->title = $this->title;
        $ticket->description = $this->description;
        $ticket->status = 'open';
        $ticket->user_id = Yii::$app->user->getId();
        return $ticket->save();
    }
}
