<?php

namespace app\models;

use Yii;
use yii\base\Model;


class RegisterForm extends Model
{
    public $name;
    public $password;
    public $email;

    /**
     * @var User
     */
    protected $user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'password', 'email'], 'required'],
            ['email', 'email'],
            [['email', 'password', 'name'],'string', 'min' => 6],
            [['email', 'password', 'name'],'string', 'max' => 60],
            ['name', 'firstLatterCapital'],
            ['password', 'lowercase'],
            ['email', 'unique', 'targetClass' => '\app\models\User']
        ];
    }

    /**
     * Check if name has capital first letter
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function firstLatterCapital($attribute, $params, $validator)
    {
        if(!preg_match('/^[A-Z]+/',$this->$attribute)){
            $validator->addError($this, $attribute, "The $attribute must start from capital letter");
        }

    }

    /**
     * Check if password in lower case
     *
     * @param $attribute
     * @param $params
     * @param $validator
     */
    public function lowercase($attribute, $params, $validator)
    {
        if(!preg_match('/^[a-z0-9_@]+$/',$this->$attribute)){
            $validator->addError($this, $attribute, "The $attribute must be in lower case");
        }

    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function register()
    {
        if ($this->validate()) {
            $this->user = new User();
            $this->user->name = $this->name;
            $this->user->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $this->user->email = $this->email;
            $this->user->save();

            return $this->user;
        }
        return   null;
    }
}
