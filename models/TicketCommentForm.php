<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Ticket;
use app\models\TicketComment;

/**
 * Class AdminTicketComment
 * @package app\models\admin
 */
class TicketCommentForm extends Model
{
    public $message;

    public function rules()
    {
        return [
            [['message'], 'required']
        ];
    }

    public function save($id, $type)
    {
        $ticketComment = new TicketComment();
        $ticketComment->message = $this->message;
        $ticketComment->ticket_id = $id;
        $ticketComment->type = $type;
        if ($ticketComment->save()) {
            $this->message = '';
        }
    }
}
