<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Ticket extends ActiveRecord
{
    static public $aliases = [
        'open' => 'Open',
        'closed' => 'Closed',
        'inProgress' => 'In progress'
    ];

    /**
     * Add timestamp to created field
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new Expression('NOW()')
            ],
        ];
    }

    /**
     * Get ticket comments
     *
     */
    public function getComments()
    {
        return $this->hasMany(TicketComment::className(), ['ticket_id' => 'id']);
    }

    /**
     * Get status alias
     *
     * @return string
     */
    public function getStatusAlias()
    {

        return self::$aliases[$this->status];
    }
}