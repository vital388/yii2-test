<?php

use yii\db\Migration;

/**
 * Class m180322_120144_add_created_at_field_to_user
 */
class m180322_120144_add_created_at_field_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'created_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'created_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180322_120144_add_created_at_field_to_user cannot be reverted.\n";

        return false;
    }
    */
}
