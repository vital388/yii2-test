<?php

use yii\db\Migration;

/**
 * Class m180326_140908_ticket_comment
 */
class m180326_140908_ticket_comment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ticket_comment', [
            'id' => $this->primaryKey(),
            'ticket_id' => $this->integer(11)->notNull(),
            'message' => $this->text()->notNull(),
            'type' => $this->string(10),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-ticket_comment-ticket_id',
            'ticket_comment',
            'ticket_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ticket_comment');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180326_140908_ticket_comment cannot be reverted.\n";

        return false;
    }
    */
}
