<?php

use yii\db\Migration;

/**
 * Class m180322_083236_init_rbac
 */
class m180322_083236_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;


        $user= $auth->createRole('user');
        $auth->add($user);


        $admin = $auth->createRole('admin');
        $auth->add($admin);

        $auth->assign($admin, 4);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180322_083236_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
