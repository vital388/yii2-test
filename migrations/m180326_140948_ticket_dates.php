<?php

use yii\db\Migration;

/**
 * Class m180326_140948_ticket_dates
 */
class m180326_140948_ticket_dates extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ticket', 'created_at', $this->dateTime());
        $this->addColumn('ticket', 'updated_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('ticket', 'created_at');
        $this->dropColumn('ticket', 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180326_140948_ticket_dates cannot be reverted.\n";

        return false;
    }
    */
}
