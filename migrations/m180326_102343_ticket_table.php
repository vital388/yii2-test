<?php

use yii\db\Migration;

/**
 * Class m180326_102343_ticket_table
 */
class m180326_102343_ticket_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('ticket', [
            'id' => $this->primaryKey(),
            'title' => $this->string(60)->notNull(),
            'description' => $this->text()->notNull(),
            'status' => $this->string(20)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
        ]);

        $this->createIndex(
            'idx-ticket-user_id',
            'ticket',
            'user_id'
        );

        $this->createIndex(
            'idx-ticket-status',
            'ticket',
            'status'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropIndex(
            'idx-user-name',
            'user'
        );

        $this->dropIndex(
            'idx-email-name',
            'user'
        );

        $this->dropIndex(
            'idx-password-name',
            'user'
        );
        $this->dropTable('user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180326_102343_ticket_table cannot be reverted.\n";

        return false;
    }
    */
}
