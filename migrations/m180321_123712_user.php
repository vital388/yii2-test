<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180321_123712_user
 */
class m180321_123712_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name' => $this->string(60)->notNull(),
            'email' => $this->string(60)->notNull()->unique(),
            'password' => $this->string(60)->notNull(),
        ]);

        $this->createIndex(
            'idx-user-name',
            'user',
            'name'
        );

        $this->createIndex(
            'idx-email-name',
            'user',
            'email'
        );

        $this->createIndex(
            'idx-password-name',
            'user',
            'password'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
        'idx-user-name',
        'user'
        );

        $this->dropIndex(
            'idx-email-name',
            'user'
        );

        $this->dropIndex(
            'idx-password-name',
            'user'
        );
        $this->dropTable('user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180321_123712_user cannot be reverted.\n";

        return false;
    }
    */
}
