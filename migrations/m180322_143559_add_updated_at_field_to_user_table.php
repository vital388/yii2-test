<?php

use yii\db\Migration;

/**
 * Class m180322_143559_add_updated_at_field_to_user_table
 */
class m180322_143559_add_updated_at_field_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'updated_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180322_143559_add_updated_at_field_to_user_table cannot be reverted.\n";

        return false;
    }
    */
}
