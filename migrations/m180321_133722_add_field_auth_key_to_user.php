<?php

use yii\db\Migration;

/**
 * Class m180321_133722_add_field_auth_key_to_user
 */
class m180321_133722_add_field_auth_key_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'auth_key', $this->string(60)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'auth_key');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180321_133722_add_field_auth_key_to_user cannot be reverted.\n";

        return false;
    }
    */
}
