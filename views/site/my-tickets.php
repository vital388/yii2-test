<?php

/* @var $this \yii\web\View */

$this->registerJs("
$('#example2').on('click', 'tbody tr', function() {
  window.location = $( this ).data('href');
  })
");

$this->title = 'Tickets';
$this->params['breadcrumbs'][] = 'Tickets';
?>
<!-- Main content -->
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Tickets</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-hover tickets-table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach (Yii::$app->user->identity->tickets as $ticket) { ?>
                        <tr data-href="/index.php?r=site/open-ticket&id=<?= $ticket->id ?>">
                            <td><?= $ticket->id ?></td>
                            <td><?= $ticket->title ?></td>
                            <td><?= $ticket->created_at ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->