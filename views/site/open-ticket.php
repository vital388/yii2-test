<?php

/* @var $ticket null|static */
/* @var $this \yii\web\View */

/* @var $commentModel \app\models\TicketCommentForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Open ticket';
?>
<div class="row">
    <div class="col-xs-6">
        <div class="open-ticket">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= $ticket->title ?></h3>
                </div>
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt>Description</dt>
                        <dd><?= $ticket->description ?></dd>
                        <dt>Status</dt>
                        <dd><?= $ticket->getStatusAlias() ?></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-6">
        <div class="box-comments">
            <?php foreach ($ticket->comments as $comment) { ?>
                <div class="box-comment">
                    <div class="comment-text">
                        <span class="username <?= $comment->type ?>">
                        <?php
                        if ($comment->type === 'user') {
                            echo 'You';
                        } else {
                            echo $comment->type;
                        }
                        ?>

                            <span class="text-muted pull-right"><?= $comment->created_at ?></span>
                        </span><!-- /.username -->
                        <?= $comment->message ?>
                    </div>
                    <!-- /.comment-text -->
                </div>
                <!-- /.box-comment -->
            <?php } ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-7">
        <?php $form = ActiveForm::begin(); ?>
        <div class="box-body">
            <?= $form->field($commentModel, 'message')->textarea(
                ['rows' => 6]
            ) ?>
            <!-- /.box-body -->
            <div class="form-group">
                <?= Html::submitButton('Comment', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


