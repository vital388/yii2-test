<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form ActiveForm */

$this->params['breadcrumbs'][] = 'Create';
$this->title = 'Create';
?>
<div class="row">
    <!-- left column -->
    <div class="col-md-6">
        <div class="create-user">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Create user</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php $form = ActiveForm::begin(); ?>
                <div class="box-body">
                    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($model, 'email')->input('email') ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <!-- /.box-body -->
                    <div class="form-group">
                        <?= Html::submitButton('Create', ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </div>
</div>
<?php
if (Yii::$app->session->hasFlash('userCreated')) {
    Modal::begin([
        'header' => '<h4>User has been created</h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    ?>
    <div class="alert alert-success" role="alert">
        <?= Yii::$app->session->getFlash('userCreated') ?>
    </div>
    <?php
    Modal::end();
    $this->registerJs("$('#modal').modal('show')");
}
?>