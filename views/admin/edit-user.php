<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form ActiveForm */

$this->params['breadcrumbs'][] = 'Edit';
$this->title = 'Edit';
?>
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <div class="edit-user">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit user</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body">
                        <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                        <?= $form->field($model, 'email')->input('email') ?>
                        <?= $form->field($model, 'password')->passwordInput() ?>
                        <!-- /.box-body -->
                        <div class="form-group">
                            <?= Html::submitButton('Edit', ['class' => 'btn btn-primary']) ?>
                            <a class="btn btn-danger" href="/?r=admin/delete-user&id=<?= $model->userId ?>">Delete
                                User</a>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
<?php
if (Yii::$app->session->hasFlash('userNotDeleted')) {
    Modal::begin([
        'header' => '<h4>User hasn\'t been deleted</h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    ?>
    <div class="alert alert-danger" role="alert">
        <?= Yii::$app->session->getFlash('userNotDeleted') ?>
    </div>
    <?php
    Modal::end();
    $this->registerJs("$('#modal').modal('show')");
} elseif (Yii::$app->session->hasFlash('userEdited')) {
    Modal::begin([
        'header' => '<h4>User has been edited</h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    ?>
    <div class="alert alert-success" role="alert">
        <?= Yii::$app->session->getFlash('userEdited') ?>
    </div>
    <?php
    Modal::end();
    $this->registerJs("$('#modal').modal('show')");
}
?>