<?php
/* @var $this yii\web\View */

/* @var $users \app\models\User[]|array|\yii\db\ActiveRecord[] */

/* @var $pages \yii\data\Pagination */

use app\assets\DataTableAsset;
use yii\bootstrap\Modal;
use yii\widgets\LinkPager;

DataTableAsset::register($this);
$this->registerJs(
    " $(function () {
    $('#example2').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : false,
      'autoWidth'   : false
    });
  })"
);

$this->title = 'Dashboard';
?>
    <h1>Sortable list of users</h1>
    <!-- Main content -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Users</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>User</th>
                            <th>Role</th>
                            <th>Created at</th>
                            <th>Edit user</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($users as $user) { ?>
                            <tr>
                                <td><?= $user->getId() ?></td>
                                <td><?= $user->name ?></td>
                                <td><?= $user->getRoles() ?></td>
                                <td><?= $user->created_at ?></td>
                                <td><a href="/?r=admin/edit-user&id=<?= $user->id ?>">Edit</a></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <?php

            echo LinkPager::widget([
                'pagination' => $pages
            ]); ?>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
<?php
if (Yii::$app->session->hasFlash('userDeleted')) {
    Modal::begin([
        'header' => '<h4>User has been deleted</h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    ?>
    <div class="alert alert-success" role="alert">
        <?= Yii::$app->session->getFlash('userDeleted') ?>
    </div>
    <?php
    Modal::end();
    $this->registerJs("$('#modal').modal('show')");
}
?>