<?php

/* @var $tickets array|\yii\db\ActiveRecord[] */

/* @var $this \yii\web\View */

use app\assets\DataTableAsset;

DataTableAsset::register($this);
$this->registerJs(
    " $(function () {
$('#example2').DataTable({
'paging'      : false,
'lengthChange': false,
'searching'   : false,
'order'       : [[ 3, 'desc' ]],
'ordering'    : true,
'info'        : false,
'autoWidth'   : false
});
})"
);
$this->registerJs("
$('#example2').on('click', 'tbody tr', function() {
  window.location = $( this ).data('href');
  })
");

$this->title = 'Tickets';
$this->params['breadcrumbs'][] = 'Tickets';
?>
<!-- Main content -->
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Tickets</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-hover tickets-table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>User id</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($tickets as $ticket) { ?>
                        <tr data-href="/index.php?r=admin/open-ticket&id=<?= $ticket->id ?>">
                            <td><?= $ticket->id ?></td>
                            <td><?= $ticket->title ?></td>
                            <td><?= $ticket->getStatusAlias() ?></td>
                            <td><?= $ticket->user_id ?></td>
                            <td><?= $ticket->created_at ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->